﻿using System.Threading.Tasks;
using JubloAps.MessageEvent.Contracts;
using JubloAps.MessageEvent.Transmitter.Managers;
using JubloAps.Shared.Interfaces;
using PostSharp.Patterns.Contracts; 

namespace JubloAps.MessageEvent.Transmitter
{
    public class MessageEventTransmitterFacilitator
    {
        private readonly MessageEventTransmitterManager _messageEventTransmitterManager;

        public MessageEventTransmitterFacilitator([NotNull] IQueueConfig queueConfig)
        {
            this._messageEventTransmitterManager = new MessageEventTransmitterManager(queueConfig);
        }

        public async Task TransmitAsync([NotNull] MessageEventContract message)
        {
            await this._messageEventTransmitterManager.TransmitAsync(message);
        }
    }
}