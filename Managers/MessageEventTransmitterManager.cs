﻿using System.Threading.Tasks;
using JubloAps.MessageEvent.Contracts;
using JubloAps.MessageEvent.Transmitter.Managers.Transmitters;
using JubloAps.Shared.Interfaces;
using PostSharp.Patterns.Contracts;

namespace JubloAps.MessageEvent.Transmitter.Managers
{
    internal class MessageEventTransmitterManager
    {
        private readonly MessageEventTransmitterTransmitter _messageEventTransmitterTransmitter;

        public MessageEventTransmitterManager([NotNull] IQueueConfig queueConfig)
        {
            this._messageEventTransmitterTransmitter = new MessageEventTransmitterTransmitter(queueConfig);
        }

        public async Task TransmitAsync([NotNull] MessageEventContract message)
        {
            await this._messageEventTransmitterTransmitter.TransmitAsync(message);
        }
    }
}