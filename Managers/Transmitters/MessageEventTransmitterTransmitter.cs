﻿using System.Text;
using System.Threading.Tasks;
using JubloAps.MessageEvent.Contracts;
using JubloAps.Shared.Interfaces;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using PostSharp.Patterns.Contracts;

namespace JubloAps.MessageEvent.Transmitter.Managers.Transmitters
{
    internal class MessageEventTransmitterTransmitter
    {
        private readonly IQueueConfig _queueConfig;

        internal MessageEventTransmitterTransmitter([NotNull] IQueueConfig queueConfig)
        {
            this._queueConfig = queueConfig;
        }

        public async Task TransmitAsync([NotNull] MessageEventContract message)
        {
            var queueClient = new QueueClient(this._queueConfig.ConnectionString(), this._queueConfig.QueueName());

            var serializeMessage = new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));

            await queueClient.SendAsync(serializeMessage).ConfigureAwait(false);

            await queueClient.CloseAsync();
        }
    }
}